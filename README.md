# Vagrant, Ansible, Kafka, Grafana, Prometheus, Graylog
In this project we use Vagrant to provision 3 VMs (Centos7) using virtualBox \
All necessary services will be deployed using Ansible

On VM-1 will be deployed:
- Kafka
- Zookeeper
- jmx & node - prometheus exporter's

On VM-2 will be deployed:
- GrayLog with dependencies (mongodb, elasticsearch) as docker containers
- Grafana
- Prometheus
- node prometheus exporter

On VM-3 will be deployed:
- https://gitlab.com/evo-test/app-producer (as docker container)
- https://gitlab.com/evo-test/app-consumer-transformer (as docker container)
- node prometheus exporter

** Grafana will be provisioned with dashboards for (kafka, prometheus, app's && vm's metrics) using prometheus as data source

** Application logs will be directed to Graylog via UDP gelf protocol

## Prerequisites

> We assume virtualbox & Vagrant is pre-installed on your OS
* https://www.virtualbox.org/wiki/Downloads
* https://www.vagrantup.com/docs/installation


## Setup

0. $ mkdir evo-test && cd $_
1. $ git clone git@gitlab.com:evo-test/deployments.git && cd deployments
2. $ vagrant plugin install vagrant-hostmanager
3. $ vagrant up


## Exposed url's
*graylog (admin:admin)*
- http://monitoring.vagrant:9000

*grafana (admin:admin)*
- http://monitoring.vagrant:3000

*prometheus UI*
- http://monitoring.vagrant:9090/graph

*jmx kafka metrics*
- http://kafka.vagrant:8080/metrics

*app microservice metrics*
- http://app.vagrant:3030/metrics
- http://app.vagrant:3031/metrics

*node-exporter vm metrics*
- http://kafka.vagrant:9100/metrics
- http://monitoring.vagrant:9100/metrics
- http://app.vagrant:9100/metrics


## TODO
- ansible vault
- ansible re-try tasks in case of connection drop/timeout
- more flexible config options (defaults) for roles
- adjust grafana dashboards to be more usefull
- elasticsearch metrics via justwatchcom/elasticsearch_exporter
- mongo metrics via percona/mongodb_exporter


## Resources used
- https://www.vagrantup.com/docs
- https://docs.ansible.com/ansible/latest/index.html
- https://moleculer.services/docs/0.14
- https://docs.graylog.org/en/3.2
- https://grafana.com/docs/grafana/latest
- https://prometheus.io/docs/introduction/overview/
- https://zookeeper.apache.org/doc/r3.4.14/index.html
- https://kafka.apache.org/documentation
- https://blog.redbranch.net/2018/04/19/zookeeper-install-on-centos-7
- https://medium.com/@mousavi310/monitor-apache-kafka-using-grafana-and-prometheus-873c7a0005e2
- https://github.com/ernesen/infra-ansible
- https://github.com/ScreamingUdder/ansible-kafka-centos
- https://github.com/sleighzy/ansible-kafka
- https://github.com/MiteshSharma/PrometheusWithGrafana
- https://github.com/prometheus/jmx_exporter
- https://github.com/prometheus/node_exporter