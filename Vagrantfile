require 'yaml'

dir = File.dirname(File.expand_path(__FILE__))
config_nodes = "#{dir}/base_config.yml"
vmconfig = YAML::load_file("#{config_nodes}")

BRIDGE_NET = vmconfig['vagrant_ip']
DOMAIN = vmconfig['vagrant_hostname']
RAM = vmconfig['vagrant_memory']

vms = [
  {
    :hostname => "kafka." + "#{DOMAIN}",
    :ip       => "#{BRIDGE_NET}" + "150",
    :ram      => 3000,
    :ansible  => {
      :playbook => "./artefacts/playbooks/playbook.yml",
      :tags     => ["zk", "kafka", "node_exporter"]
    }
  },
  {
    :hostname => "monitoring." + "#{DOMAIN}",
    :ip       => "#{BRIDGE_NET}" + "151",
    :ram      => 3000,
    :ansible  => {
      :playbook => "./artefacts/playbooks/playbook.yml",
      :tags     => ["grafana", "prometheus", "node_exporter", "graylog"]
    }
  },
  {
    :hostname => "app." + "#{DOMAIN}",
    :ip       => "#{BRIDGE_NET}" + "152",
    :ram      => "#{RAM}",
    :ansible  => {
      :playbook => "./artefacts/playbooks/playbook.yml",
      :tags     => ["app", "node_exporter"]
    }
  }
]

Vagrant.configure(2) do |config|
  config.hostmanager.enabled = true
  config.hostmanager.manage_host = true
  config.hostmanager.manage_guest = false #not required, if `natdnshostresolver` mode is used
  config.vm.synced_folder ".", vmconfig["vagrant_directory"], type: "virtualbox"
  vms.each do |vm|
    config.vm.define vm[:hostname] do |node|
      node.vm.box = vmconfig["vagrant_box"]
      node.vm.box_version = vmconfig["vagrant_box_version"]
      node.vm.hostname = vm[:hostname]
      node.vm.network "private_network", ip: vm[:ip] 
      node.vm.provider "virtualbox" do |vb|
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
        vb.cpus = vmconfig["vagrant_cpu"]
        vb.memory = vm[:ram]
        vb.name = vm[:hostname]
        if (!vm[:ansible].nil? && !vm[:ansible][:playbook].nil?)
          node.vm.provision "Bootstrap ansible @ #{vm[:hostname]}", type: "ansible_local" do |ansible|
            # ansible.verbose = "-vvvv"
            # ansible.raw_arguments = "--list-tasks"
            ansible.raw_arguments = ["--force-handler"]
            ansible.playbook = vm[:ansible][:playbook]
            ansible.inventory_path = "./artefacts/inventories/vagrant_virtualbox"
            if (!vm[:ansible][:tags].nil?)
              ansible.tags = vm[:ansible][:tags]
            end
          end
        end
      end
    end
  end
end